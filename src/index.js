import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Uploadselfie from './Uploadselfie';
import Uploadkyc from './Uploadkyc';
import Upload_aadhar from './Upload_aadhar';
import Upload_pan from './Upload_pan';
import Upload_passport from './Upload_passport';
import Upload_voterid from './Upload_voterid';
import EditAPI from './EditAPI';
import EditList from './EditList';
import Details from './Details';
import Options from './Options';
import { HashRouter, Route } from 'react-router-dom';


import * as serviceWorker from './serviceWorker';

const routing = (
    <HashRouter>
    <div>
     
      <Route exact path="/" component={App} />
      <Route path="/uploadselfie" component={Uploadselfie} />
      <Route path="/uploadkyc" component={Uploadkyc} />
      <Route path="/upload_aadhar" component={Upload_aadhar} />
      <Route path="/upload_pan" component={Upload_pan} />
      <Route path="/upload_passport" component={Upload_passport} />
      <Route path="/upload_voterid" component={Upload_voterid} />
      <Route path="/editapi" component={EditAPI} />
      <Route path="/editlist" component={EditList} />
      <Route path="/details" component={Details} />
      <Route path="/options" component={Options} />
     
    </div>
  </HashRouter>
);

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
