import React, {
    Component
} from "react";
import "./App.css";
import "./custom.css";
import jQuery from "jquery";
import {
    Row,
    Col
} from "reactstrap";

import "bootstrap/dist/css/bootstrap.css";
import Header from './Header';
import { Cookies } from "react-cookie";
import config from './config';
const cookies = new Cookies();
var DocumentId, APIkey;

class Uploadselfie extends Component {
    constructor(props) {
        super(props);
        this.uploadSelfie = this.uploadSelfie.bind(this);

   this.state = {
        apikey: cookies.get('cookieAPIkey'),
        documentkey: cookies.get('cookieDocumentid')
     }

     var myapiCookie = cookies.get('cookieAPIkey')

    if (myapiCookie != null) 
    {
  var x = cookies.get('cookieAPIkey');
   APIkey = x.key;
  }
   else
   {
     alert("Please enter API Key");
     window.location.href='/#/editapi';
   }


     var mydocumentidCookie = cookies.get('cookieDocumentid')

    if (mydocumentidCookie != null) 
    {
  var xy = cookies.get('cookieDocumentid');
   DocumentId = xy.key;
  }
   else
   {
     alert("Please upload the KYC document");
     window.location.href='/#/uploadkyc';
   }

   /*var photodocumentidCookie = cookies.get('cookiephotoDocumentid')

    if (photodocumentidCookie != null) 
    {
  var x = cookies.get('cookiephotoDocumentid');
   photo_DocumentId = x.key;
   console.log("photo cookiephotoDocumentid", photo_DocumentId);
  }
   else
   {
     alert("Please upload the KYC document");
     window.location.href='/#/uploadkyc';
   }*/

  }

    uploadSelfie(e) {
       
        jQuery(function($) {
            function readFile() {
                if (this.files && this.files[0]) {
                    var ImgfileReader = new FileReader();

                    ImgfileReader.addEventListener("load", function(e) {
                        document.getElementById("photo").src = e.target.result;
                        var dataUri = e.target.result;
                       

                        jQuery.ajax({
                            url: config.upload_selfie_api,
                            method: "POST",
                            crossDomain: true,
                            headers: {
                                Authorization: APIkey,
                                "Content-Type": "application/json"
                            },
                            data: JSON.stringify({
                                dataUri: dataUri,
                                documentId : DocumentId
                              
                            }),

                            success: function(response) {

                                jQuery("#error-message").hide();
                          

                                if (response) {
                                    document.getElementById("upload-photo-section").style.display = "none";
                                    document.getElementById("errorresponsetab").style.display = "none";
                                    document.getElementById("responsetab").style.display = "block";
                                    document.getElementById("front_back").style.display = "none";

                                    var doc_id = response.kycDetails.documentId;
                                    
                                     jQuery('#card_doc_id').html(doc_id);


                                     if (response.similar === true)
                                     {
                                    document.getElementById("success-msg").innerHTML =
                                    "<h5>Face Matched: True</h5>";
                                    document.getElementById("face-tab").style.display = "block";
                                    var c_kycface = response.kycFaceDataUri;
                                    
                                    var c_selfieFace = response.selfieFaceDataUri;
                                     
                                    var c_cp = response.confidencePercent;
                                      
                                    var c_cg = response.confidenceGroup;
                                     
                                    document.getElementById("d_face1").src = c_kycface;
                                    document.getElementById("d_face2").src = c_selfieFace;
                                      jQuery('#kyc_cp').html(c_cp);
                                      jQuery('#kyc_cg').html(c_cg);

                                     }
                                     else
                                     {
                                        document.getElementById("success-err-msg").innerHTML =
                                    "<h5>Face Matched: False</h5>"; 
                                     }

                                    if (response.kycDetails) 
                                      {

                                        if (response.kycDetails.type === "aadhaar") 
                                      {
                                      document.getElementById("records_pan").style.display = "none";
                                       document.getElementById("records_passport").style.display = "none";
                                        document.getElementById("records_voterid").style.display = "none";
                                    var c_type = response.kycDetails.type;
                                     
                                     var c_aadhaarno = response.kycDetails.details.aadhaar;
                                    
                                     var c_dob = response.kycDetails.details.dob;
                                    
                                     var c_doi = response.kycDetails.details.doi;
                                    
                                     var c_father = response.kycDetails.details.father;
                                    
                                     var c_husband = response.kycDetails.details.husband;
                                    
                                     var c_gender = response.kycDetails.details.gender;
                                   
                                     var c_mother = response.kycDetails.details.mother;
                                     
                                     var c_name = response.kycDetails.details.name;
                                    
                                     var c_yob = response.kycDetails.details.yob;
                                    
                                     var c_qr = response.kycDetails.details.qr;
                                    
                                     var c_addresscareof = response.kycDetails.details.address.care_of;
                                    
                                     var c_district = response.kycDetails.details.address.district;
                                     
                                     var c_line = response.kycDetails.details.address.line;
                                                                                                                                                                                         
                                     var c_house = response.kycDetails.details.address.house;
                                     
                                     var c_pin = response.kycDetails.details.address.pin;
                                                                          
                                     var c_state = response.kycDetails.details.address.state;
                                     
                                     var c_cvalue = response.kycDetails.details.address.value;
                                     
                                     var c_phone = response.kycDetails.details.phone;
                                 
                                    

                                    document.getElementById("page-title").style.display = "none";
                                       jQuery('#d_type').html(c_type);
                                       jQuery('#d_cardnumber').html(c_aadhaarno);
                                       jQuery('#d_dob').html(c_dob);
                                       jQuery('#d_doi').html(c_doi);
                                       jQuery('#d_father').html(c_father);
                                       jQuery('#d_husband').html(c_husband);
                                       jQuery('#d_gender').html(c_gender);
                                       jQuery('#d_mother').html(c_mother);
                                       jQuery('#d_name').html(c_name);
                                       jQuery('#d_yob').html(c_yob);
                                       jQuery('#d_qr').html(c_qr);
                                       jQuery('#d_addr_careof').html(c_addresscareof);
                                       jQuery('#d_addr_district').html(c_district);
                                       jQuery('#d_addr_line').html(c_line);
                                       jQuery('#d_addr_house').html(c_house);
                                       jQuery('#d_addr_pin').html(c_pin);
                                       jQuery('#d_addr_state').html(c_state);
                                       jQuery('#d_addr_value').html(c_cvalue);
                                       jQuery('#d_phonenumber').html(c_phone);
                                        
                                        }
                   
                                        if (response.kycDetails.type === "pan") 
                                      {
                                        document.getElementById("records_aadhar").style.display = "none";
                                        document.getElementById("records_passport").style.display = "none";
                                        document.getElementById("records_voterid").style.display = "none";

                                     var p_type = response.kycDetails.type;
                                     
                                     var p_panno = response.kycDetails.details.pan_no;
                                    
                                     var p_dob = response.kycDetails.details.date;
                                    
                                     var p_name = response.kycDetails.details.name;
                                     
                                     var p_father = response.kycDetails.details.father;
                                     
                                     var p_dateofissue = response.kycDetails.details.date_of_issue;
                                     
                                  
                                     
                                     document.getElementById("page-title").style.display = "none";
                                     jQuery('#pan_type').html(p_type);
                                     jQuery('#pan_number').html(p_panno);
                                     jQuery('#pan_dob').html(p_dob);
                                     jQuery('#pan_name').html(p_name);
                                     jQuery('#pan_father').html(p_father);
                                      jQuery('#pan_dateisssue').html(p_dateofissue);

                                      }

                                       if (response.kycDetails.type === "passport") 
                                      {
                                        document.getElementById("records_aadhar").style.display = "none";
                                        document.getElementById("records_pan").style.display = "none";
                                        document.getElementById("records_voterid").style.display = "none";

                                       var pass_type = response.kycDetails.type;
                                       var pass_det_country = response.kycDetails.details.country_code;
                                       var pass_det_dob = response.kycDetails.details.dob;
                                       var pass_det_doe = response.kycDetails.details.doe;
                                       var pass_det_doi = response.kycDetails.details.doi;
                                       var pass_det_gender = response.kycDetails.details.gender;
                                       var pass_det_name = response.kycDetails.details.name;
                                       var pass_det_nationality = response.kycDetails.details.nationality;
                                       var pass_det_passport_num = response.kycDetails.details.passport_num;
                                       var pass_det_place_of_birth = response.kycDetails.details.place_of_birth;
                                       var pass_det_place_of_issue = response.kycDetails.details.place_of_issue;
                                       var pass_det_surname = response.kycDetails.details.surname;
                                       var pass_det_mrz1 = response.kycDetails.details.mrz.line1;
                                       var pass_det_mrz2 = response.kycDetails.details.mrz.line2;
                                       var pass_det_address = response.kycDetails.details.address;
                                       var pass_det_father = response.kycDetails.details.father;
                                       var pass_det_mother = response.kycDetails.details.mother;
                                       var pass_det_file_num = response.kycDetails.details.file_num;
                                       var pass_det_old_doi = response.kycDetails.details.old_doi;
                                       var pass_det_old_passport_num = response.kycDetails.details.old_passport_num;
                                       var pass_det_old_place_of_issue = response.kycDetails.details.old_place_of_issue;
                                       var pass_det_pin = response.kycDetails.details.pin;
                                       var pass_det_spouse = response.kycDetails.details.spouse;

                                       jQuery('#table_pass_type').html(pass_type);
                                       jQuery('#table_pass_country').html(pass_det_country);
                                       jQuery('#table_pass_dob').html(pass_det_dob);
                                       jQuery('#table_pass_doe').html(pass_det_doe);
                                       jQuery('#table_pass_gender').html(pass_det_gender);
                                       jQuery('#table_pass_name').html(pass_det_name);
                                       jQuery('#table_pass_nation').html(pass_det_nationality);
                                       jQuery('#table_pass_passport_num').html(pass_det_passport_num);
                                       jQuery('#table_pass_place_of_birth').html(pass_det_place_of_birth);
                                       jQuery('#table_pass_place_of_issue').html(pass_det_place_of_issue);
                                       jQuery('#table_pass_surname').html(pass_det_surname);
                                       jQuery('#table_pass_line1').html(pass_det_mrz1);
                                       jQuery('#table_pass_line2').html(pass_det_mrz2);
                                       jQuery('#table_pass_address').html(pass_det_address);
                                       jQuery('#table_pass_father').html(pass_det_father);
                                       jQuery('#table_pass_mother').html(pass_det_mother);
                                       jQuery('#table_pass_file_num').html(pass_det_file_num);
                                       jQuery('#table_pass_old_doi').html(pass_det_old_doi);
                                       jQuery('#table_pass_old_passport_num').html(pass_det_old_passport_num);
                                       jQuery('#table_pass_old_place_of_issue').html(pass_det_old_place_of_issue);
                                       jQuery('#table_pass_pin').html(pass_det_pin);
                                       jQuery('#table_pass_spouse').html(pass_det_spouse);
                                      }

                                        if (response.kycDetails.type === "voterid") 
                                      {

                                       document.getElementById("records_aadhar").style.display = "none";
                                        document.getElementById("records_pan").style.display = "none";
                                        document.getElementById("records_passport").style.display = "none";

                                        var voter_type = response.kycDetails.type;
                                        var voter_details_voterid = response.kycDetails.details.voterid;
                                        var voter_details_age = response.kycDetails.details.age;
                                        var voter_details_name = response.kycDetails.details.name;
                                        var voter_details_relation = response.kycDetails.details.relation;
                                        var voter_details_gender = response.kycDetails.details.gender;
                                        var voter_details_dob = response.kycDetails.details.dob;
                                        var voter_details_doi = response.kycDetails.details.doi;
                                        var voter_details_aao = response.kycDetails.details.aao;
                                        var voter_details_yob = response.kycDetails.details.yob;
                                        var voter_details_constituency = response.kycDetails.details.constituency;
                                        var voter_details_address = response.kycDetails.details.address.address_line;
                                        var voter_details_city = response.kycDetails.details.address.city;
                                        var voter_details_state = response.kycDetails.details.address.state;
                                        var voter_details_district = response.kycDetails.details.address.district;
                                        var voter_details_pin = response.kycDetails.details.address.pin;

                                        jQuery('#table_voter_type').html(voter_type);
                                        jQuery('#table_voter_voterid').html(voter_details_voterid);
                                        jQuery('#table_voter_age').html(voter_details_age);
                                        jQuery('#table_voter_name').html(voter_details_name);
                                        jQuery('#table_voter_relation').html(voter_details_relation);
                                        jQuery('#table_voter_gender').html(voter_details_gender);
                                        jQuery('#table_voter_dob').html(voter_details_dob);
                                        jQuery('#table_voter_doi').html(voter_details_doi);
                                        jQuery('#table_voter_aao').html(voter_details_aao);
                                        jQuery('#table_voter_constituency').html(voter_details_constituency);
                                        jQuery('#table_voter_address_line').html(voter_details_address);
                                        jQuery('#table_voter_city').html(voter_details_city);
                                        jQuery('#table_voter_state').html(voter_details_state);
                                        jQuery('#table_voter_district').html(voter_details_district);
                                        jQuery('#table_voter_pin').html(voter_details_pin);

                                      }


                                  }

                                }
                            },

                            beforeSend: function() {
                                jQuery('.upload-loader').show();
                                jQuery('#container-circles').hide();
                                jQuery('.react-html5-camera-photo').hide();

                            },
                            complete: function() {
                                jQuery('.upload-loader').hide();
                                jQuery('#container-circles').show();
                                jQuery('.react-html5-camera-photo').show();
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) 
                            {
                                document.getElementById("upload-photo-section").style.display = "none";
                                document.getElementById("errorresponsetab").style.display = "block";
                                document.getElementById("responsetab").style.display = "none";
                                document.getElementById("front_back").style.display = "none";
                                var err = eval("(" + XMLHttpRequest.responseText + ")");
                                var err_msg = err;
                                var eString = JSON.stringify(err_msg, null, 4);
                                

                                jQuery('#responseerror').html(eString);
                                document.getElementById("error-message").innerHTML =
                                    "<h5>Error Response</h5>";

                            }

                        });
                    });

                    ImgfileReader.readAsDataURL(this.files[0]);
                }
            }

            document
                .getElementById("browse-file")
                .addEventListener("change", readFile);

        });
    }

   

    render() {
        return ( 
            <div className = "Uploadselfie" >
            <Header />

            <div className ="page-title" id="page-title">
            <h5>Upload a Selfie</h5>
            </div>

            <div id="video-section" style={{ display: "none" }} >

            </div>

            <div id = "upload-photo-section">
            <div className = "ios-pic">
            <div >
            <img className = "display-img"
            id = "pic"
            src = ""
            alt = ""/>
            <img className = "display-img-url"
            id = "photo"
            src = ""
            alt = ""/>
            </div>

            <div className = "upload-loader" > </div>

            <div >
            <label className = "fileContainer">
            Upload a photo <
            input id = "browse-file"
            type = "file"
            onClick = {
                this.uploadSelfie
            }
            /> 
            </label>

            </div>

            </div> </div>

            <div className ="goback" id="front_back">
               <span className = "try-tab success-btn">
                     <a href="/#/uploadkyc">Back</a>
                </span>
            </div>

            <div>
            <p id = "error-message"> </p>
            </div>

            <div>
                 <p id="success-message"></p>
            </div>

            <div id="face-tab" style={{ display: "none" }}>
             <Row>
            <Col></Col>
              <Col>
              <div className="table-responsive">
            <table className="table" id="kyc-selfie-table">
              <tr>
              <th>KYC Face</th>
              <th>Selfie Face</th>
              <th>Confidence Percent</th>
              <th>Confidence Group </th>
              </tr>

              <tr>
               <td id="face1">
               <img src="" alt="face or document" id="d_face1" />
               </td>
              
               <td id="face2">
               <img src="" alt="face or document" id="d_face2" />
               </td>

               <td id="kyc_cp">
               </td>

               <td id="kyc_cg">
               </td>

               </tr>

            </table>
            </div>

              </Col>
                <Col></Col>
                </Row>
            </div>

              <div id="responsetab" style={{ display: "none" }}>
              <div id="success-msg">
              </div>
              <div id="success-err-msg">
              </div>
        <Row>
            <Col></Col>
            <Col>
             <h5> Document Details (<span id="card_doc_id"></span>) </h5>
             <p></p>
             <table className="table aadhaar" id="records_aadhar" border='1'>

               <tr>
               <td>Card Type</td>
               <td id="d_type"></td>
               </tr>

               <tr>
               <td>Card Number</td>
               <td id="d_cardnumber"></td>
               </tr>

               <tr>
               <td>DOB</td>
               <td id="d_dob"></td>
               </tr>

               <tr>
               <td>DOI</td>
               <td id="d_doi"></td>
               </tr>

               <tr>
               <td>Father</td>
               <td id="d_father"></td>
               </tr>

               <tr>
               <td>Husband</td>
               <td id="d_husband"></td>
               </tr>

               <tr>
               <td>Gender</td>
               <td id="d_gender"></td>
               </tr>

               <tr>
               <td>Mother</td>
               <td id="d_mother"></td>
               </tr>

               <tr>
               <td>Name</td>
               <td id="d_name"></td>
               </tr>

               <tr>
               <td>YOB</td>
               <td id="d_yob"></td>
               </tr>

               <tr>
               <td>QR</td>
               <td id="d_qr"></td>
               </tr>

                 <tr>
               <td>Address care of</td>
               <td id="d_addr_careof"></td>
               </tr>

               <tr>
               <td>Address house</td>
               <td id="d_addr_house"></td>
               </tr>

               <tr>
               <td>Address line</td>
               <td id="d_addr_line"></td>
               </tr>

               <tr>
               <td>Address district</td>
               <td id="d_addr_district"></td>
               </tr>

              <tr>
               <td>Address state</td>
               <td id="d_addr_state"></td>
               </tr>

               <tr>
               <td>Address pin</td>
               <td id="d_addr_pin"></td>
               </tr>

               <tr>
               <td>Address value</td>
               <td id="d_addr_value"></td>
               </tr>

               <tr>
               <td>Phone number</td>
               <td id="d_phonenumber"></td>
               </tr>

              
              </table>

             <table className="table pan" id="records_pan" border='1'>
              
               <tr>
               <td>Card type</td>
               <td id="pan_type"></td>
               </tr>

               <tr>
               <td>Card Number</td>
               <td id="pan_number"></td>
               </tr>

               <tr>
               <td>DOB</td>
               <td id="pan_dob"></td>
               </tr>

               <tr>
               <td>Name</td>
               <td id="pan_name"></td>
               </tr>

               <tr>
               <td>Father</td>
               <td id="pan_father"></td>
               </tr>

                <tr>
               <td>Card date of issue</td>
               <td id="pan_dateisssue"></td>
               </tr>

            </table>

            <table className="table passport" id="records_passport" border='1'>

               <tr>
               <td>Card Type</td>
               <td id="table_pass_type"></td>
               </tr>

               <tr>
               <td>Country Code</td>
               <td id="table_pass_country"></td>
               </tr>

               <tr>
               <td>DOB</td>
               <td id="table_pass_dob"></td>
               </tr>

               <tr>
               <td>DOE</td>
               <td id="table_pass_doe"></td>
               </tr>

               <tr>
               <td>DOI</td>
               <td id="table_pass_doi"></td>
               </tr>

               <tr>
               <td>Gender</td>
               <td id="table_pass_gender"></td>
               </tr>

               <tr>
               <td>Name</td>
               <td id="table_pass_name"></td>
               </tr>

               <tr>
               <td>Nationality</td>
               <td id="table_pass_nation"></td>
               </tr>

               <tr>
               <td>Passport Number</td>
               <td id="table_pass_passport_num"></td>
               </tr>

               <tr>
               <td>Place of birth</td>
               <td id="table_pass_place_of_birth"></td>
               </tr>

               <tr>
               <td>Place of issue</td>
               <td id="table_pass_place_of_issue"></td>
               </tr>

               <tr>
               <td>Surname</td>
               <td id="table_pass_surname"></td>
               </tr>

               <tr>
               <td>Line 1</td>
               <td id="table_pass_line1"></td>
               </tr>

              <tr>
               <td>Line 2</td>
               <td id="table_pass_line2"></td>
               </tr>

                <tr>
               <td>Address</td>
               <td id="table_pass_address"></td>
               </tr>

                <tr>
               <td>Father</td>
               <td id="table_pass_father"></td>
               </tr>

                <tr>
               <td>Mother</td>
               <td id="table_pass_mother"></td>
               </tr>

                <tr>
               <td>File number</td>
               <td id="table_pass_file_num"></td>
               </tr>

                <tr>
               <td>Old Doi</td>
               <td id="table_pass_old_doi"></td>
               </tr>

                <tr>
               <td>Old Passport Num</td>
               <td id="table_pass_old_passport_num"></td>
               </tr>

                <tr>
               <td>Old Place of Issue</td>
               <td id="table_pass_old_place_of_issue"></td>
               </tr>

                <tr>
               <td>Pin</td>
               <td id="table_pass_pin"></td>
               </tr>

                <tr>
               <td>Spouse</td>
               <td id="table_pass_spouse"></td>
               </tr>
     

              </table>

              <table className="table tab_voterid" id="records_voterid" border='1'>

               <tr>
               <td>Type</td>
               <td id="table_voter_type"></td>
               </tr>

               <tr>
               <td>Voter Id</td>
               <td id="table_voter_voterid"></td>
               </tr>


               <tr>
               <td>Age</td>
               <td id="table_voter_age"></td>
               </tr>


               <tr>
               <td>Type</td>
               <td id="table_voter_type"></td>
               </tr>

               <tr>
               <td>Name</td>
               <td id="table_voter_name"></td>
               </tr>

               <tr>
               <td>Relation</td>
               <td id="table_voter_relation"></td>
               </tr>

               <tr>
               <td>Gender</td>
               <td id="table_voter_gender"></td>
               </tr>

               <tr>
               <td>DOB</td>
               <td id="table_voter_dob"></td>
               </tr>

               <tr>
               <td>DOI</td>
               <td id="table_voter_doi"></td>
               </tr>


               <tr>
               <td>AAO</td>
               <td id="table_voter_aao"></td>
               </tr>

               <tr>
               <td>YOB</td>
               <td id="table_voter_yob"></td>
               </tr>


               <tr>
               <td>Constituency</td>
               <td id="table_voter_constituency"></td>
               </tr>

                 <tr>
               <td>Address</td>
               <td id="table_voter_address_line"></td>
               </tr>

                 <tr>
               <td>City</td>
               <td id="table_voter_city"></td>
               </tr>

                <tr>
               <td>State</td>
               <td id="table_voter_state"></td>
               </tr>


               <tr>
               <td>District</td>
               <td id="table_voter_district"></td>
               </tr>

                <tr>
               <td>Pin</td>
               <td id="table_voter_pin"></td>
               </tr>

              </table>

                <pre id="responsecode" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

        <Row className="refresh-btn">
         <Col></Col>
         <Col>
         <div>
               <span className = "try-tab success-btn">
                     <a href="/#/uploadselfie" onClick={() => window.location.reload()}>Back</a>
                </span>
                </div>
               <div className ="goback">
                 <span className = "try-tab">
                     <a href="/#/options">Goto Upload Document</a>
                </span>
                </div>
          </Col>
          <Col></Col>
         </Row>
    </div>

    <div id="errorresponsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col>
                <pre id="responseerror" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

         <Row className="refresh-btn">
          <Col></Col>
           <Col>
           <span className = "try-tab">
                     <a href="/#/uploadselfie" onClick={() => window.location.reload()}>Try Again</a>
                 </span>
            </Col>
           <Col></Col>
          </Row>
    </div>

            <div id = "try-btn"
            style = {
                {
                    display: "none"
                }
            } >
            <span className = "try-tab">
            <a href = "/" > Try Again </a> 
            </span> </div>
</div>
        );
    }
}

export default Uploadselfie;