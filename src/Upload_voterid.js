import React, {
    Component
} from "react";
import "./App.css";
import "./custom.css";
import Camera, {
    FACING_MODES,
    IMAGE_TYPES
} from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import jQuery from "jquery";
import {
    Row,
    Col
} from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";
import Header from './Header';
import { Cookies } from "react-cookie";
import config from './config';
const cookies = new Cookies();
var APIkey;
var SelectedCard,selectcardCookie;

class Upload_voterid extends Component {
    constructor(props) {
        super(props);
        this.uploadSelfie = this.uploadSelfie.bind(this);

  this.state = {
        apikey: cookies.get('cookieAPIkey'),
        selectcard: cookies.get('cookieanycard')
     }

     var myapiCookie = cookies.get('cookieAPIkey')

    if (myapiCookie != null) 
    {
  var x = cookies.get('cookieAPIkey');
   APIkey = x.key;
  
  }
   else
   {
     alert("Please enter API Key");
     window.location.href='/#/editapi';
   }

   selectcardCookie = cookies.get('cookieanycard');
   console.log(selectcardCookie);

    if (selectcardCookie != null) 
    {
  var xy = cookies.get('cookieanycard');
   SelectedCard = xy.key;
  }
  }

    uploadSelfie(e) {
       
        jQuery(function($) {
            function readFile() {
                if (this.files && this.files[0]) {
                    var ImgfileReader = new FileReader();

                    ImgfileReader.addEventListener("load", function(e) {
                        document.getElementById("photo").src = e.target.result;
                        var dataUri = e.target.result;

                        jQuery.ajax({
                            url: config.upload_kyc_api,
                            method: "POST",
                            crossDomain: true,
                            headers: {
                                Authorization: APIkey,
                                "Content-Type": "application/json"
                            },
                            data: JSON.stringify({
                                dataUri: dataUri,
                                 attributes: "text",
                                imageCrops: "front,front_top,back,front_bottom,photo,signature,qrcode",
                                type: "voterid"
                            }),

                            success: function(response) {

                                jQuery("#error-message").hide();
                               

                                if (response) {
                                    console.log(response);
                                    document.getElementById("upload-photo-section").style.display = "none";
                                    document.getElementById("errorresponsetab").style.display = "none";
                                    document.getElementById("responsetab").style.display = "block";
                                    document.getElementById("front_back").style.display = "none";

                                    var codeString = JSON.stringify(response, null, 4);
                                    jQuery('#responsecode').html(codeString);
                                    

                                    if (response.result) 
                                      {
                                        if (response.result.length === 1) 
                                      {
                                    var documentid = response.result[0].documentId;
                                    
                                    var CookieDate = new Date();
                                    CookieDate.setFullYear(CookieDate.getFullYear() +10);
                                    var expdate = CookieDate.toGMTString();
                                    
                                    cookies.set('cookieDocumentid', {key: documentid}, { path: '/' }, { expires: expdate })
                                    
                                    if (response.result[0].type === "voterid")
                                    {
                                        document.getElementById("success-message-1").innerHTML =
                                    "<h5>Your document uploaded successfully, Please follow next step to match with photo..</h5>";
                                    var doc_id = response.result[0].documentId;
                                    
                                     jQuery('#card_doc_id').html(doc_id);
                                     console.log(doc_id);

                                     var jString = JSON.stringify(response.result[0].imageCrops);
                                      var jdata = JSON.parse(jString);
                                      var trHTML = '';
                                          jQuery.each(jdata, function(key, value)
                                          {
                                            trHTML += 
                                           '<tr><td>' + key +  
                                           '</td><td>' + '<img  src= '+ value +'  />' +
                                           '</td></tr>';       
                                         });
                                     jQuery('#kyc-document-image-table').append(trHTML); 
                                     
                                    }
                                    else
                                    {
                                         document.getElementById("success-message-2").innerHTML =
                                    "<h5>ID Not Matched, Please Upload the valid photo copy of Aadhar Card or PAN Card.</h5>";
                                     document.getElementById("typematch").style.display = "block";
                                     document.getElementById("responsetab").style.display = "none";
                                    document.getElementById("next-step").style.display = "none";
                                    }

                                     
                                     
                                       if (response.result[0].type === "voterid") 
                                      {

                                        var voter_type = response.result[0].type;
                                        var voter_details_voterid = response.result[0].details.voterid;
                                        var voter_details_age = response.result[0].details.age;
                                        var voter_details_name = response.result[0].details.name;
                                        var voter_details_relation = response.result[0].details.relation;
                                        var voter_details_gender = response.result[0].details.gender;
                                        var voter_details_dob = response.result[0].details.dob;
                                        var voter_details_doi = response.result[0].details.doi;
                                        var voter_details_aao = response.result[0].details.aao;
                                        var voter_details_yob = response.result[0].details.yob;
                                        var voter_details_constituency = response.result[0].details.constituency;
                                        var voter_details_address = response.result[0].details.address.address_line;
                                        var voter_details_city = response.result[0].details.address.city;
                                        var voter_details_state = response.result[0].details.address.state;
                                        var voter_details_district = response.result[0].details.address.district;
                                        var voter_details_pin = response.result[0].details.address.pin;

                                        jQuery('#table_voter_type').html(voter_type);
                                        jQuery('#table_voter_voterid').html(voter_details_voterid);
                                        jQuery('#table_voter_age').html(voter_details_age);
                                        jQuery('#table_voter_name').html(voter_details_name);
                                        jQuery('#table_voter_relation').html(voter_details_relation);
                                        jQuery('#table_voter_gender').html(voter_details_gender);
                                        jQuery('#table_voter_dob').html(voter_details_dob);
                                        jQuery('#table_voter_doi').html(voter_details_doi);
                                        jQuery('#table_voter_aao').html(voter_details_aao);
                                        jQuery('#table_voter_constituency').html(voter_details_constituency);
                                        jQuery('#table_voter_address_line').html(voter_details_address);
                                        jQuery('#table_voter_city').html(voter_details_city);
                                        jQuery('#table_voter_state').html(voter_details_state);
                                        jQuery('#table_voter_district').html(voter_details_district);
                                        jQuery('#table_voter_pin').html(voter_details_pin);

                                      }

                                      }
                                  }                 
                                   
                                }
                            },

                            beforeSend: function() {
                                jQuery('.upload-loader').show();
                                jQuery('#container-circles').hide();
                                jQuery('.react-html5-camera-photo').hide();

                            },
                            complete: function() {
                                jQuery('.upload-loader').hide();
                                jQuery('#container-circles').show();
                                jQuery('.react-html5-camera-photo').show();
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) 
                            {
                                document.getElementById("upload-photo-section").style.display = "none";
                                document.getElementById("errorresponsetab").style.display = "block";
                                document.getElementById("responsetab").style.display = "none";
                                document.getElementById("front_back").style.display = "none";
                                var err = eval("(" + XMLHttpRequest.responseText + ")");
                                var err_msg = err;
                                var eString = JSON.stringify(err_msg, null, 4);
                                

                                jQuery('#responseerror').html(eString);
                                document.getElementById("error-message").innerHTML =
                                    "<h5>Error Response</h5>";

                            }

                        });
                    });

                    ImgfileReader.readAsDataURL(this.files[0]);
                }
            }

            document
                .getElementById("browse-file")
                .addEventListener("change", readFile);

        });
    }


componentDidMount()
      {
    if(SelectedCard === "anycard")
  {
  document.getElementById("page_title_anycard").style.display = "block";
  }

  if(SelectedCard === "aadhaar")
  {
  document.getElementById("page_title_aadharcard").style.display = "block";
  }

  if(SelectedCard === "pan")
  {
  document.getElementById("page_title_pancard").style.display = "block";
  }

  if(SelectedCard === "passport")
  {
  document.getElementById("page_title_passport_card").style.display = "block";
  }

  if(SelectedCard === "voterid")
  {
  document.getElementById("page_title_voter_card").style.display = "block";
  }

   jQuery("#doc-image-link").click(function() 
   {
     document.getElementById("doc-image-link").style.backgroundColor = "#f3f3f3";
     document.getElementById("doc-text-link").style.backgroundColor = "#fff";
      document.getElementById("responsecode-image-tab").style.display = "block";
      document.getElementById("responsecode-text-tab").style.display = "none";
   });

   jQuery("#doc-text-link").click(function() 
   { 
    document.getElementById("doc-text-link").style.backgroundColor = "#f3f3f3";
    document.getElementById("doc-image-link").style.backgroundColor = "#fff";
      document.getElementById("responsecode-image-tab").style.display = "none";
      document.getElementById("responsecode-text-tab").style.display = "block";
   });
}

    render() {
        return ( 
            <div className = "Uploadkyc" >
            <Header />

            <div className ="page-title">
            <h5 id="page_title_anycard" style={{ display: "none" }}>Upload Aadhar or PAN Card </h5>
            <h5 id="page_title_aadharcard" style={{ display: "none" }}>Upload Aadhar Card </h5>
            <h5 id="page_title_pancard" style={{ display: "none" }}>Upload PAN Card</h5>
            <h5 id="page_title_passport_card" style={{ display: "none" }}>Upload Passport</h5>
            <h5 id="page_title_voter_card" style={{ display: "none" }}>Upload Voter ID</h5>
            </div>

            <div id = "upload-photo-section">
            <div className = "ios-pic">
            <div >
            <img className = "display-img"
            id = "pic"
            src = ""
            alt = ""/>
            <img className = "display-img-url"
            id = "photo"
            src = ""
            alt = ""/>
            </div>

            <div className = "upload-loader" > </div>

            <div >
            <label className = "fileContainer">
            Upload a document <
            input id = "browse-file"
            type = "file"
            onClick = {
                this.uploadSelfie
            }
            /> 
            </label>

            </div>

            </div> </div>

            <div className ="goback" id="front_back">
               <span className = "try-tab success-btn">
                     <a href="/#/options">Back</a>
                </span>
            </div>

            <div>
            <p id = "error-message"> </p>
            </div>

            <div>
                 <p id="success-message"></p>
            </div>
             <div>
                 <p id="success-message-1"></p>
            </div>
            <div>
                 <p id="success-message-2"></p>
            </div>

            <div>
                 <p id="success-message-passport"></p>
            </div>

            <div id="responsetab" style={{ display: "none" }}>

            <Row className="refresh-btn">
         <Col></Col>
         <Col id="next-step">
            <div>
               <span className = "try-tab primary-btn">
                     <a href="/#/uploadselfie">Next: Match With Photo</a>
                </span>
              </div>
               <div className ="goback">
               <span className = "try-tab success-btn">
                     <a href="/#/uploadkyc" onClick={() => window.location.reload()}>Back</a>
                </span>
              </div>
               
             </Col>
             <Col></Col>
            </Row>

             <div>
              <h5> Document Id (<span id="card_doc_id"></span>)</h5>
             </div>
            
            <div id="doc-link-section">
                <div id="doc-text-link">
                Document Text Details
                </div>

               <div id="doc-image-link"> 
               Document Image Section
               </div>
             </div>

           <div id="responsecode-image-tab" style={{ display: "none" }}>
             <Row>
               <Col> </Col>

               <Col>
               <p></p>
               <h5>Document Image Section</h5>
               <p></p>
               <table id="kyc-document-image-table">
                </table>
               </Col>

                <Col> </Col>

                </Row>
                </div>

            <div id="responsecode-text-tab" >
        <Row>
        <Col> </Col>
        <Col>
               <p></p>
               <h5> Document Text Details  </h5>
               <p></p>
       

              <table className="table tab_voterid" id="records_voterid" border='1'>

               <tr>
               <td>Type</td>
               <td id="table_voter_type"></td>
               </tr>

               <tr>
               <td>Voter Id</td>
               <td id="table_voter_voterid"></td>
               </tr>


               <tr>
               <td>Age</td>
               <td id="table_voter_age"></td>
               </tr>


               <tr>
               <td>Type</td>
               <td id="table_voter_type"></td>
               </tr>

               <tr>
               <td>Name</td>
               <td id="table_voter_name"></td>
               </tr>

               <tr>
               <td>Relation</td>
               <td id="table_voter_relation"></td>
               </tr>

               <tr>
               <td>Gender</td>
               <td id="table_voter_gender"></td>
               </tr>

               <tr>
               <td>DOB</td>
               <td id="table_voter_dob"></td>
               </tr>

               <tr>
               <td>DOI</td>
               <td id="table_voter_doi"></td>
               </tr>


               <tr>
               <td>AAO</td>
               <td id="table_voter_aao"></td>
               </tr>

               <tr>
               <td>YOB</td>
               <td id="table_voter_yob"></td>
               </tr>


               <tr>
               <td>Constituency</td>
               <td id="table_voter_constituency"></td>
               </tr>

                 <tr>
               <td>Address</td>
               <td id="table_voter_address_line"></td>
               </tr>

                 <tr>
               <td>City</td>
               <td id="table_voter_city"></td>
               </tr>

                <tr>
               <td>State</td>
               <td id="table_voter_state"></td>
               </tr>


               <tr>
               <td>District</td>
               <td id="table_voter_district"></td>
               </tr>

                <tr>
               <td>Pin</td>
               <td id="table_voter_pin"></td>
               </tr>

              </table>


               </Col>

                <Col> </Col>
             </Row>
             </div>

        <Row className="responsecode-tab">
            <Col></Col>
            <Col>
                <pre id="responsecode" style={{ display: "none" }} className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

        
    </div>

<div id="typematch" style={{ display: "none" }}>
   <Row className="refresh-btn">
         <Col></Col>
         <Col>
             <span className = "try-tab">
                     <a href="/#/upload_voterid" onClick={() => window.location.reload()}>Back</a>
                 </span>
          </Col>
          <Col></Col>
         </Row>
 </div>

    <div id="errorresponsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col>
                <pre id="responseerror" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

         <Row className="refresh-btn">
          <Col></Col>
           <Col>
           <span className = "try-tab">
                     <a href="/#/upload_voterid" onClick={() => window.location.reload()}>Try Again</a>
                 </span>
            </Col>
           <Col></Col>
          </Row>
    </div>

            <div id = "try-btn"
            style = {
                {
                    display: "none"
                }
            } >
            <span className = "try-tab">
            <a href = "/" > Try Again </a> 
            </span> </div>
</div>
        );
    }
}

export default Upload_voterid;